<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }

    /**
     * convert -coalesce -background white 'GV094Xr.gif' JPEG:tmp00/some%05d.jpg
     * avconv -i tmp00/some%05d.jpg -r 24 out.mp4
     */
    public function actionExe()
    {
//        TODO:
// 1 path + $gif
// 2 if_exist $gif.mp4 -> rm $gif.mp4
//
        $gif = "data/GV094Xr.gif";
        $command = "mkdir -p /tmp/000";
        exec($command, $op, $return);
        $command = "convert -coalesce -background white '$gif' JPEG:/tmp/000/some%05d.jpg";
        exec($command, $op, $return);
        $command = "avconv -i /tmp/000/some%05d.jpg -r 24 $gif.mp4";
        exec($command, $op, $return);
        $command = "rm -rf /tmp/000/";
        exec($command);
    }
}
